package question29;

import java.util.Scanner;

public class NumeralSum {
	// QUESTION 29
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Input : ");
		String word = scan.nextLine();
		int sum = 0;
		for (int n = 0; n < word.length(); n++) {
			if (Character.isDigit(word.charAt(n))) {
				sum = sum + Character.getNumericValue(word.charAt(n));
			}
		}
		System.out.println("Total : "+sum);
	}
}

/*OUTPUT

Input : Konsi8kj1k3jk4m2n1
Total : 19


*/