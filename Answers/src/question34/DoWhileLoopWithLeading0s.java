package question34;

public class DoWhileLoopWithLeading0s {
	// QUESTION 34
	public static void main(String args[]) {	
		int num = 10;
		int n = 1;
		do {
			int odd = ((n * 2) - 1);
			int even = n * 2;
			String oddValue = String.format("%03d", odd);
			String evenValue = String.format("%03d", even);
			System.out.println(oddValue + ".0" + "," + evenValue + ".0");
			n = n + 1;
		} while (n <= num / 2);
	}
}

/*OUTPUT

001.0,002.0
003.0,004.0
005.0,006.0
007.0,008.0
009.0,010.0
 
 */