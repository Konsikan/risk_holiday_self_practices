package question02;

public class DiamondFilled {
	// QUESTION 02
	public static void main(String args[]) {
		int num = 10;
		int n = 0;
		do {
			for (int i = 0; i <= num; i++) {
				if (n < num / 2) {
					if (i < ((num / 2) - (n + 1))) {
						System.out.print(" ");
					} else if (i <= (((num / 2) - 1))) {
						System.out.print("/");
					}
					if ((i >= (num / 2)) && (i <= (num / 2) + n)) {
						System.out.print("\\");
					} else if (i > ((num - n))) {
						System.out.print(" ");
					}
					System.out.print("");
				} else {
					if (i < (n - (num / 2))) {
						System.out.print(" ");
					} else if (i <= ((num / 2) - 1)) {
						System.out.print("\\");
					}
					if ((i >= (num / 2) && (i <= (num - (n - (num / 2) + 1))))) {
						System.out.print("/");
					} else if (i >= (num - (n - (num / 2)))) {
						System.out.print("");
					}
				}
			}
			System.out.println();
			n = n + 1;
		} while (n < num);
	}
}

/* OUTPUT
   
    /\
   //\\ 
  ///\\\  
 ////\\\\  
/////\\\\\ 
\\\\\/////
 \\\\////
  \\\///
   \\//
    \/
    
 */