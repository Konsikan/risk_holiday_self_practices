package question32;

import java.util.Scanner;

public class RotatingVowels {
	// QUESTION 32
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("ENTER A WORD\t\t: ");
		String word = scan.nextLine();
		String chWord = "";
		for (int i = 0; i < word.length(); i++) {
			char com = word.charAt(i);
			if ((com != 'A') && (com != 'E') && (com != 'I') && (com != 'O') && (com != 'U') && (com != 'a')
					&& (com != 'e') && (com != 'i') && (com != 'o') && (com != 'u')) {
				chWord = chWord + com;
			} else {
				switch (com) {
				case 'A':
					chWord = chWord + "E";
					break;
				case 'a':
					chWord = chWord + "e";
					break;
				case 'E':
					chWord = chWord + "I";
					break;
				case 'e':
					chWord = chWord + "i";
					break;
				case 'I':
					chWord = chWord + "O";
				case 'i':
					chWord = chWord + "o";
					break;
				case 'O':
					chWord = chWord + "U";
					break;
				case 'o':
					chWord = chWord + "u";
					break;
				case 'U':
					chWord = chWord + "A";
					break;
				case 'u':
					chWord = chWord + "a";
					break;
				}
			}
		}
		System.out.println("AFTER ROTATTING VOWELS\t: " + chWord);
	}
}

/*OUTPUT

ENTER A WORD		: Louis
AFTER ROTATTING VOWELS	: Luaos

*/