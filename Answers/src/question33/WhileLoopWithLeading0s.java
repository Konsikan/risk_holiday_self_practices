package question33;

public class WhileLoopWithLeading0s {
	// QUESTION 33
	public static void main(String args[]) {
		int num = 10;
		int n = 1;
		while (n <= num/2) {
			int even=n*2;
			String value=String.format("%03d", even);
			System.out.println(value+":");
			n = n + 1;
		}
	}
}

/*OUTPUT

002:
004:
006:
008:
010:

*/