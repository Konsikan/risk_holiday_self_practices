package question19;

import java.util.Scanner;

public class TreasureHunting2ndMethod {
	// QUESTION 19
	public static void main(String args[]) {
		final int treasure = 80;
		int num = 10;
		int previousDistance = 100;
		System.out.println();
		System.out.println("| LETS FIND THE TREASURE |");
		System.out.println();
		for (int i = 1; i <= num; i++) {
			Scanner scan = new Scanner(System.in);
			System.out.print("GUESS " + i + " : ");
			int input = scan.nextInt();
			int distance = Math.abs(treasure - input);
			if (input == treasure) {
				System.out.println("\t | You have found the treasure! |");
				break;
			}
			if ((distance > 0) && (distance <= 3)) {
				System.out.println("\t| The treasure is very close. | ");
				TreasureHunting2ndMethod demo = new TreasureHunting2ndMethod();
				demo.ComparePreviousDistance(treasure, input, previousDistance, distance, i);
			}
			if ((distance > 3) && (distance <= 6)) {
				System.out.println("\t| The treasure is somewhat close. | ");
				TreasureHunting2ndMethod demo = new TreasureHunting2ndMethod();
				demo.ComparePreviousDistance(treasure, input, previousDistance, distance, i);
			}
			if (distance > 6) {
				System.out.println("\t| The treasure is not close. | ");
				TreasureHunting2ndMethod demo = new TreasureHunting2ndMethod();
				demo.ComparePreviousDistance(treasure, input, previousDistance, distance, i);
			}
			if ((i == 10) && (input != treasure)) {
				System.out.println();
				System.out.println("| You have not found the treasure! |");
				System.out.println("|| TREASURE : " + treasure + " ||");
				break;
			}
			previousDistance = distance;
		}
	}

	public void ComparePreviousDistance(int treasure, int input, int previousDistance, int distance, int i) {
		if (i > 1) {
			if (previousDistance > distance) {
				System.out.println("\t| You are closer. | ");
			}
			if (previousDistance < distance) {
				System.out.println("\t| You are farther. | ");
			}
			if (previousDistance == distance) {
				System.out.println("\t| The same distance. | ");
			}
		}
	}
}

/*
 
OUTPUT1

| LETS FIND THE TREASURE |

GUESS 1 : 45
	| The treasure is not close. | 
GUESS 2 : 48
	| The treasure is not close. | 
	| You are closer. | 
GUESS 3 : 78
	| The treasure is very close. | 
	| You are closer. | 
GUESS 4 : 74
	| The treasure is somewhat close. | 
	| You are farther. | 
GUESS 5 : 80
	 | You have found the treasure! |

OUTPUT2


| LETS FIND THE TREASURE |

GUESS 1 : 15
	| The treasure is not close. | 
GUESS 2 : 48
	| The treasure is not close. | 
	| You are closer. | 
GUESS 3 : 49
	| The treasure is not close. | 
	| You are closer. | 
GUESS 4 : 59
	| The treasure is not close. | 
	| You are closer. | 
GUESS 5 : 72
	| The treasure is not close. | 
	| You are closer. | 
GUESS 6 : 75
	| The treasure is somewhat close. | 
	| You are closer. | 
GUESS 7 : 71
	| The treasure is not close. | 
	| You are farther. | 
GUESS 8 : 76
	| The treasure is somewhat close. | 
	| You are closer. | 
GUESS 9 : 78
	| The treasure is very close. | 
	| You are closer. | 
GUESS 10 : 79
	| The treasure is very close. | 
	| You are closer. |

| You have not found the treasure! |
|| TREASURE : 80 ||

 */
