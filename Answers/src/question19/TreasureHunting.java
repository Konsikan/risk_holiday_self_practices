package question19;

import java.util.Scanner;

public class TreasureHunting {
	// QUESTION 19
	public static void main(String args[]) {
		final int treasure = 80;
		int num = 10;
		int n = 1;
		int minDistance1 = 100;
		do {
			System.out.println();
			System.out.println("ROUND : " + n);
			System.out.println();
			for (int i = 1; i <= num; i++) {
				Scanner scan = new Scanner(System.in);
				System.out.print("GUESS " + i + " : ");
				int input = scan.nextInt();
				int distance1 = Math.abs(treasure - input);
				if (n == 1) {
					if (distance1 < minDistance1) {
						minDistance1 = distance1;
					}
					if (input == treasure) {
						System.out.println("\t | You have found the treasure! |");
						break;
					}
					if ((distance1 > 0) && (distance1 <= 3)) {
						System.out.println("\t| The treasure is very close. | ");
					}
					if ((distance1 > 3) && (distance1 <= 6)) {
						System.out.println("\t| The treasure is somewhat close. | ");
					}
					if (distance1 > 6) {
						System.out.println("\t| The treasure is not close. | ");
					}
				}
				int distance2 = Math.abs(treasure - input);
				int minDistance2 = Math.abs(distance2 - minDistance1);
				if (n == 2) {
					if (input == treasure) {
						System.out.println("\t | You have found the treasure! |");
						break;
					}
					if ((i == 10) && (input != treasure)) {
						System.out.println("\t| You have not found the treasure! |");
						System.out.println("\t|| TREASURE : " + treasure + " ||");
						break;
					}
					if ((minDistance2 > 0) && (minDistance2 <= 3)) {
						System.out.println("\t| You are closer. | ");
					}
					if ((minDistance2 > 3)) {
						System.out.println("\t| You are farther. | ");
					}
					if (distance2 == minDistance1) {
						System.out.println("\t| The same distance. | ");
					}
				}
			}
			n = n + 1;
		} while (n <= 2);

	}
}

/* OUTPUT 1

ROUND : 1

GUESS 1 : 78
	| The treasure is very close. | 
GUESS 2 : 79
	| The treasure is very close. | 
GUESS 3 : 45
	| The treasure is not close. | 
GUESS 4 : 71
	| The treasure is not close. | 
GUESS 5 : 48
	| The treasure is not close. | 
GUESS 6 : 75
	| The treasure is somewhat close. | 
GUESS 7 : 72
	| The treasure is not close. | 
GUESS 8 : 77
	| The treasure is very close. | 
GUESS 9 : 74
	| The treasure is somewhat close. | 
GUESS 10 : 73
	| The treasure is not close. | 

ROUND : 2

GUESS 1 : 71
	| You are farther. | 
GUESS 2 : 78
	| You are closer. | 
GUESS 3 : 79
	| The same distance. | 
GUESS 4 : 75
	| You are farther. | 
GUESS 5 : 80
	 | You have found the treasure! |

*/

/* OUTPUT 2


ROUND : 1

GUESS 1 : 45
	| The treasure is not close. | 
GUESS 2 : 75
	| The treasure is somewhat close. | 
GUESS 3 : 79
	| The treasure is very close. | 
GUESS 4 : 14
	| The treasure is not close. | 
GUESS 5 : 75
	| The treasure is somewhat close. | 
GUESS 6 : 48
	| The treasure is not close. | 
GUESS 7 : 78
	| The treasure is very close. | 
GUESS 8 : 79
	| The treasure is very close. | 
GUESS 9 : 72
	| The treasure is not close. | 
GUESS 10 : 75
	| The treasure is somewhat close. | 

ROUND : 2

GUESS 1 : 78
	| You are closer. | 
GUESS 2 : 74
	| You are farther. | 
GUESS 3 : 76
	| You are closer. | 
GUESS 4 : 72
	| You are farther. | 
GUESS 5 : 78
	| You are closer. | 
GUESS 6 : 79
	| The same distance. | 
GUESS 7 : 74
	| You are farther. | 
GUESS 8 : 78
	| You are closer. | 
GUESS 9 : 79
	| The same distance. | 
GUESS 10 : 79
	| You have not found the treasure! |
	|| TREASURE : 80 ||
	
*/