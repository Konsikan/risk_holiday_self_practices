package question06;

import java.util.Scanner;

public class ComputeTaxAndTotal {
	// QUESTION 06
	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.print("SUBTOTAL : ");
		String strsubTotal = scan.nextLine();
		System.out.print("TAX PERCENT (%) : ");
		double taxPercent = scan.nextInt();

		double intSubTotal = Integer.parseInt(strsubTotal);
		double tax = (intSubTotal * taxPercent) / 100;
		double total = intSubTotal + tax;
		
		String strTotal = String.valueOf(total);

		System.out.println(" The Subtotal = " + intSubTotal + " dollars and " + intSubTotal + " cents.");

		System.out.println(" The tax rate = " + taxPercent + " percent.");

		System.out.println(" The total = " + total + " dollars and cents ");
	}
}

/*

Need To Edit the code...................................

*/