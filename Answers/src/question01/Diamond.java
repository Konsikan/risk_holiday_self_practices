package question01;

public class Diamond {
	// QUESTION 01
	public static void main(String args[]) {
		int num = 10;
		int n = 0;
		do {
			for (int i = 0; i <= num; i++) {
				if (n < num / 2) {
					if (i == ((num / 2) - (n + 1))) {
						System.out.print("/");
					}
					if (i == ((num / 2) + (n - 1))) {
						System.out.print("\\");
					} else {
						System.out.print(" ");
					}
				} else {
					if (i == (n - (num / 2))) {
						System.out.print("\\");
					}
					if (i == (n + (num / 2)) - (2 * ((n + 1) - (num / 2)))) {
						System.out.print("/");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
			n = n + 1;
		} while (n < num);
	}
}

/* OUTPUT

    /\      
   /  \     
  /    \    
 /      \   
/        \  
\        /  
 \      /   
  \    /    
   \  /     
    \/ 

*/