package question30;

import java.util.Scanner;

public class CyclicShifts {
	// QUESTION 30
	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter your input line: ");
		String w = scan.nextLine();
		System.out.print("Enter the shift value k: ");
		int k = scan.nextInt();
		String fw = "";
		String lw = "";
		for (int i = 0; i <= (k - 1); i++) {
			fw = fw + w.charAt(i);
		}
		for (int i = k; i <= (w.length() - 1); i++) {
			lw = lw + w.charAt(i);
		}
		CyclicShifts demo = new CyclicShifts();
		demo.print(w, k, fw, lw);
	}

	public void print(String w, int k, String fw, String lw) {
		if (k > w.length()) {
			System.out.println("The value is invalid and stop.");
		}
		System.out.println();
		System.out.println("The " + k + " cyclic shift of");
		System.out.println("'" + w + "'");
		System.out.println("is");
		if (k == 0 || k == w.length()) {
			System.out.println("'" + w + "'");
		} else {
			System.out.println("'" + lw + fw + "'");
		}
	}
}

/*OUTPUT

Enter your input line: How are you?
Enter the shift value k: 4

The 4 cyclic shift of
'How are you?'
is
'are you?How '

*/
