package question20;

import java.util.Scanner;

public class Movements2D {
	// QUESTION 20
	public static void main(String[] args) {
		System.out.println("UP-U | DOWN-D | LEFT-L | RIGHT-R");
		Scanner scan = new Scanner(System.in);
		System.out.print("ENTER THE COMMAND: ");
		String command = scan.nextLine();
		String toUpper = command.toUpperCase();
		System.out.print("(0,0) ");
		int x=0;
		int y=0;
		for (int n = 0; n < toUpper.length(); n++) {
			char com = toUpper.charAt(n);
			switch (com) {
			case 'U':
				y=y+1;
				System.out.print("--> ("+x+","+y+") ");
				break;
			case 'D':
				y=y-1;
				System.out.print("--> ("+x+","+y+") ");
				break;
			case 'L':
				x=x-1;
				System.out.print("--> ("+x+","+y+") ");
				break;
			case 'R':
				x=x+1;
				System.out.print("--> ("+x+","+y+") ");
				break;
			}
		}
	}
}

/*OUTPUT

UP-U | DOWN-D | LEFT-L | RIGHT-R
ENTER THE COMMAND: UDLLRR
(0,0) --> (0,1) --> (0,0) --> (-1,0) --> (-2,0) --> (-1,0) --> (0,0) 

*/