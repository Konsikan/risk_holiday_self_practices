package question22;

import java.util.Scanner;

public class ConnectingStringValues {
	// QUESTION 22
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("WORD 1 : ");
		String word1 = scan.nextLine();
		System.out.print("WORD 2 : ");
		String word2 = scan.nextLine();
		char fw1=word1.charAt(0);
		char fw2=word2.charAt(0);
		char lw1=word1.charAt(word1.length()-1);
		char lw2=word2.charAt(word2.length()-1);
		System.out.print("RESULT : " + connect(word1,word2,fw1,fw2,lw1,lw2));
	}

	public static boolean connect(String word1, String word2,char fw1,char fw2,char lw1,char lw2) {
		return (lw1==fw2) || (lw2==fw1) ||(word1.length()==word2.length()) ;
	}
}
 /*OUTPUT

WORD 1 : Srilanka
WORD 2 : America
RESULT : false

*/