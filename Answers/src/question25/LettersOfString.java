package question25;

public class LettersOfString {
	// QUESTION 25
	public static void main(String[] args) {
		String word = "hurricanes";
		int length = word.length();
		for (int i = 0; i < length; i++) {
			System.out.println(word.charAt(i));
		}
	}
}

/*OUTPUT

h 
u 
r 
r 
i 
c 
a 
n 
e 
s
 
 */