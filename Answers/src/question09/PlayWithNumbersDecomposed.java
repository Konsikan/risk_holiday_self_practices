package question09;

import java.util.Scanner;

public class PlayWithNumbersDecomposed {
	// QUESTION 09
	public void task1(int num1, int num2) {
		Scanner scan1 = new Scanner(System.in);
		System.out.print("ENTER TWO INTEGERS : ");
		num1 = scan1.nextInt();
		num2 = scan1.nextInt();
		System.out.println("");
		int result1 = num1 + num2;
		int result2 = num1 - num2;
		int result3 = num1 * num2;
		int result4 = num1 / num2;
		int result5 = num1 % num2;
		System.out.println("a + b  IS EQUAL TO   " + result1);
		System.out.println("a - b  IS EQUAL TO   " + result2);
		System.out.println("a * b  IS EQUAL TO   " + result3);
		System.out.println("a / b  IS EQUAL TO   " + result4);
		System.out.println("a % b  IS EQUAL TO   " + result5);
		System.out.println("");
	}

	public void task2(int n1, int n2, int n3) {
		Scanner scan2 = new Scanner(System.in);
		System.out.print("ENTER THREE INTEGERS : ");
		n1 = scan2.nextInt();
		n2 = scan2.nextInt();
		n3 = scan2.nextInt();
		System.out.println("");
		int result1 = (n1 - n2) / n3;
		int result2 = (n1 - n3) / n2;
		int result3 = (n2 - n1) / n3;
		int result4 = (n2 - n3) / n1;
		int result5 = (n3 - n1) / n2;
		int result6 = (n3 - n2) / n1;
		System.out.println("( a - b ) / c IS EQUAL TO   " + result1);
		System.out.println("( a - c ) / b IS EQUAL TO   " + result2);
		System.out.println("( b - a ) / c IS EQUAL TO   " + result3);
		System.out.println("( b - c ) / a IS EQUAL TO   " + result4);
		System.out.println("( c - a ) / b IS EQUAL TO   " + result5);
		System.out.println("( c - b ) / a IS EQUAL TO   " + result6);
		System.out.println("");
	}

	public static void main(String args[]) {
		PlayWithNumbersDecomposed demo = new PlayWithNumbersDecomposed();
		int num1 = 0;
		int num2 = 0;
		int n1 = 0;
		int n2 = 0;
		int n3 = 0;
		demo.task1(num1, num2);
		demo.task2(n1, n2, n3);
	}
}

/*OUTPUT

ENTER TWO INTEGERS : 1000435 345

a + b  IS EQUAL TO   1000780
a - b  IS EQUAL TO   1000090
a * b  IS EQUAL TO   345150075
a / b  IS EQUAL TO   2899
a % b  IS EQUAL TO   280

ENTER THREE INTEGERS : 34325 79 -40

( a - b ) / c IS EQUAL TO   -856
( a - c ) / b IS EQUAL TO   435
( b - a ) / c IS EQUAL TO   856
( b - c ) / a IS EQUAL TO   0
( c - a ) / b IS EQUAL TO   -435
( c - b ) / a IS EQUAL TO   0

*/