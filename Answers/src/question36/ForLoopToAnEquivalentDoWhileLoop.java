package question36;

public class ForLoopToAnEquivalentDoWhileLoop {
	// QUESTION 34
	public static void main(String args[]) {
		// USING FOR LOOP
		/*for (int index = 1; index <= 15; index = index + 3) {
			System.out.println(index);
		}*/
		// USING DO WHILE LOOP
		int index = 1;
		do {
			System.out.println(index);
			index=index + 3;
		} while (index <= 15);
	}
}

/*OUTPUT

1
4
7
10
13

*/