package question03;

public class XwithXs {
	// QUESTION 03
	public static void main(String args[]) {
		int num = 10;
		int n = 1;
		String printingSymbole = "x";
		String space = " ";
		do {
			for (int i = 1; i <= num; i++) {
				if (i == n || i == (num - n)) {
					System.out.print(printingSymbole);
				} else {
					System.out.print(space);
				}
			}
			System.out.println();
			n = n + 1;
		} while (n < num);
	}
}

/*OUTPUT
 
x       x 
 x     x  
  x   x   
   x x    
    x     
   x x    
  x   x   
 x     x  
x       x

 */
