package question15;

public class DoubleForLoop {
	// QUESTION 15
	public static void main(String args[]) {
		int num = 10;
		for (int n = 1; n <= num; n = n + 2) {
			for (int j = 1; j <= num - 1; j++) {
				if (j >= n) {
					System.out.print(j);
				}
			}
			System.out.println();
		}
	}

}

/*OUTPUT

123456789
3456789
56789
789
9

*/
