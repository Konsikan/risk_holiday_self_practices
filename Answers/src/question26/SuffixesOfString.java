package question26;

public class SuffixesOfString {
	// QUESTION 26
	public static void main(String[] args) {
		String word = "hurricanes";
		int length = word.length();
		for (int line = 0; line <= length; line++) {
			for (int i = line; i <= line; i++) {
				System.out.print(word.substring(i));
			}
			System.out.println();
		}
	}
}

/*OUTPUT

hurricanes
urricanes
rricanes
ricanes
icanes
canes
anes
nes
es
s

*/
