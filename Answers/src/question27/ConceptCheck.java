package question27;

public class ConceptCheck {
	// QUESTION 27
	public static void main(String[] args) {
		String s = "Mississippi";
		System.out.println(s.length());
		System.out.println(s.indexOf("si"));
		System.out.println(s.toUpperCase().indexOf("si"));
		System.out.println(s.toLowerCase().indexOf("si"));
		System.out.println(s.substring(0, s.indexOf("i")));
		System.out.println(s.substring(s.lastIndexOf("i")));
	}
}

/*OUTPUT

11
3
-1
3
M
i

*/
