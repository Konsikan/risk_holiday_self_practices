package question21;

import java.util.Scanner;

public class ReceiveAndPrint {
	// QUESTION 21
	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.print("STRING : ");
		String s = scan.nextLine();
		System.out.print("INTEGER : ");
		int m = scan.nextInt();
		System.out.print("DOUBLE : ");
		double d = scan.nextDouble();
		System.out.println(s);
		System.out.println(m);
		System.out.println(d);
		System.out.println(m + d + s);
		System.out.println(m + s + d);
		System.out.println(s + m + d);
	}
}

/*OUTPUT

KONSIKAN
5
2.5
7.5KONSIKAN
5KONSIKAN2.5
KONSIKAN52.5

*/