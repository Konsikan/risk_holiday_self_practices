package question07;

public class SpeedingFine {
	// QUESTION 07
	public static void main(String args []) {
		int speed1 = 70; int limit1 = 35;
		int speed2 = 50; int limit2 = 25;
		int speed3 = 80; int limit3 = 45;
		int fine1 = (speed1 - limit1) * 20;
		int fine2 = (speed2 - limit2) * 20;
		int fine3 = (speed3 - limit3) * 20;
		System.out.println(
				"The fine for driving at " + speed1 + " mph on a " + limit1 + " mph road is " + fine1 + " dollars. ");
		System.out.println(
				"The fine for driving at " + speed2 + " mph on a " + limit2 + " mph road is " + fine2 + " dollars. ");
		System.out.println(
				"The fine for driving at " + speed3 + " mph on a " + limit3 + " mph road is " + fine3 + " dollars. ");
	}
}

/*OUTPUT

The fine for driving at 70 mph on a 35 mph road is 700 dollars. 
The fine for driving at 50 mph on a 25 mph road is 500 dollars. 
The fine for driving at 80 mph on a 45 mph road is 700 dollars. 

*/