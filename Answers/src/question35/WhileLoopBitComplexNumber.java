package question35;

public class WhileLoopBitComplexNumber {
	// QUESTION 35
	public static void main(String args[]) {
		int n = 10;
		int j = n;
		while (j >= 0) {
			int value = (int) Math.pow(2, j);
			String out = String.format("%04d", value);
			System.out.println("+" + out + "?");
			j = j - 1;
		}
	}
}

/*OUTPUT

+1024?
+0512?
+0256?
+0128?
+0064?
+0032?
+0016?
+0008?
+0004?
+0002?
+0001?

*/