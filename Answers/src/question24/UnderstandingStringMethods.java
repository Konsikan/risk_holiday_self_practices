package question24;

public class UnderstandingStringMethods {
	// QUESTION 24
	public static void main(String[] args) {
		String w = "Singin�_in_the_rain";
		String pat = "in";
		System.out.println(w.indexOf(pat));
		System.out.println(w.indexOf(pat,3));
		System.out.println(w.indexOf(pat,6));
		System.out.println(w.lastIndexOf(pat));
		System.out.println(w.length());
		System.out.println(w.toUpperCase());
		System.out.println(w.charAt(0));
	}
}

/*OUTPUT

1
4
8
17
19
SINGIN�_IN_THE_RAIN
S

*/