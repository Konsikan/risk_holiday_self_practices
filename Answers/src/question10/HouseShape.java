package question10;

public class HouseShape {
	// QUESTION 10
	public void bottom(int num) {
		int n = 1;
		do {
			for (int i = 1; i <= num * 2; i++) {
				if (n <= num) {
					if (i == (num - (n - 1))) {
						System.out.print("/");
					}
					if (i == (num + (n - 1))) {
						System.out.print("\\");
					}
					System.out.print(" ");
				}
			}
			System.out.println();
			n = n + 1;
		} while (n <= num);
	}

	public void Center(int num) {
		int n = 1;
		do {
			for (int i = 1; i <= num * 2; i++) {
				if ((n == 1) || (n == num + 1)) {
					if ((i == 2) || (i == (num * 2) - 1)) {
						System.out.print("+");
					} else {
						System.out.print("-");
					}
				}
				if ((n == 2) || (n == (num))) {

					if ((i == 2) || (i == (num * 2) - 1)) {
						System.out.print("|");
					} else {
						System.out.print(" ");
					}
				}
				if ((n == 3) || (n == (num - 1))) {

					if ((i == 2) || (i == (num * 2) - 2)) {
						System.out.print("|");
					}
					if ((i == 5) || (i == 6)) {
						System.out.print("-");
					} else {
						System.out.print(" ");
					}
				}
				if ((n == (num / 2) + 1)) {
					if ((i == num - 1) || (i == num + 2) || (i == 2) || (i == (num * 2) - 1)) {
						System.out.print("|");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
			n = n + 1;
		} while (n <= num + 1);
	}

	public void Message(String args[]) {
		System.out.println("This is my house!");
	}

	public static void main(String args[]) {
		HouseShape demo = new HouseShape();
		int num = 6;
		demo.bottom(num);
		demo.Center(num);
		demo.Message(args);
	}
}

/*OUTPUT
  
     /\       
    /  \      
   /    \     
  /      \    
 /        \   
/          \  
-+--------+-
 |        | 
 |   --   |   
 |  |  |  | 
 |   --   |   
 |        | 
-+--------+-
This is my house!

 */