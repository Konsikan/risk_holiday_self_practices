package question28;

import java.util.Scanner;

public class StringReverse {
	// QUESTION 28
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter an input String : ");
		String s = scan.nextLine();
		String total = "";
		int length = s.length();
		for (int i = length - 1; i >= 0; i--) {
			total = total + s.charAt(i);
		}
		System.out.println("The reverse of " + s + " is " + total + ". ");
	}
}

/*OUTPUT

The reverse of Computer-Programming is gnimmargorP-retupmoC

*/
