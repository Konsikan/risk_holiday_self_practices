package question16;

public class AnotherDoubleForLoop {
	// QUESTION 16
	public static void main(String args[]) {
		int num = 10;
		for (int n = 0; n < num / 2; n++) {
			for (int j =( num - n - 1); j > n; j--) {
				System.out.print(j);
			}
			System.out.println();
		}
	}
}

/*OUTPUT

987654321
8765432
76543
654
5

*/