package question14;

import java.util.Scanner;

public class AllPositive {
	// QUESTION 14
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("VALUE A : ");
		double ValueA = scan.nextInt();
		System.out.print("VALUE B : ");
		double ValueB = scan.nextInt();
		System.out.print("VALUE C : ");
		double ValueC = scan.nextInt();
		System.out.print("IS ALLPOSITIVE : " + AllPositive(ValueA, ValueB, ValueC));
	}

	public static boolean AllPositive (double ValueA, double ValueB, double ValueC) {
		return (ValueA > 0) && (ValueB > 0) && (ValueC > 0);
	}
}

/*OUTPUT

VALUE A : 45
VALUE B : 14
VALUE C : -4
IS ALLPOSITIVE : false

 */