package question12;

import java.util.Scanner;

public class IsValidTriangle {
	// QUESTION 12
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("SIDE 1 : ");
		double side1 = scan.nextInt();
		System.out.print("SIDE 2 : ");
		double side2 = scan.nextInt();
		System.out.print("SIDE 3 : ");
		double side3 = scan.nextInt();
		System.out.print("IS VALID TRIANGLE : " + ValidTriangle(side1, side2, side3));
	}

	public static boolean ValidTriangle(double side1, double side2, double side3) {
		return (side1 + side2 > side3) && (side1 + side3 > side2) && (side2 + side3 > side1);
	}
}

/*OUTPUT

SIDE 1 : 10
SIDE 2 : 12
SIDE 3 : 10
IS VALID TRIANGLE: true

*/