package question11;

public class SlashesWithMethodCalls {
	// QUESTION 11
	public void line1(String[] args) {
		int num = 10;
		for (int i = 1; i <= num + 2; i++) {
			System.out.print("/ ");
		}
		System.out.println("");
	}

	public void line2(String[] args) {
		int num = 10;
		for (int j = 1; j <= num + 1; j++) {
			System.out.print(" /");
		}
		System.out.println("");
	}

	public static void main(String args[]) {
		SlashesWithMethodCalls demo = new SlashesWithMethodCalls();
		demo.line1(args); demo.line2(args);
		demo.line1(args); demo.line2(args);
		demo.line1(args); demo.line2(args);
		demo.line1(args); demo.line2(args);
		demo.line1(args); demo.line2(args);
	}
}

/*OUTPUT

/ / / / / / / / / / / / 
 / / / / / / / / / / /
/ / / / / / / / / / / / 
 / / / / / / / / / / /
/ / / / / / / / / / / / 
 / / / / / / / / / / /
/ / / / / / / / / / / / 
 / / / / / / / / / / /
/ / / / / / / / / / / / 
 / / / / / / / / / / /

*/

