package question0405;

public class Slashes {
	// QUESTION 04
	public static void main(String args[]) {
		int num = 10;
		String Slashes = "/";
		for (int t = 1; t <= num; t++) {
			if (t % 2 == 0) {
				for (int i = 1; i <= (num + 1); i++) {
					System.out.print(" "+Slashes);
				}
				System.out.println();
			} else {
				for (int i = 1; i <= (num + 2); i++) {
					System.out.print(Slashes+" ");
				}
				System.out.println();
			}
		}
	}
}

/*OUTPUT

/ / / / / / / / / / / /  
 / / / / / / / / / / / 
/ / / / / / / / / / / /  
 / / / / / / / / / / / 
/ / / / / / / / / / / /  
 / / / / / / / / / / / 
/ / / / / / / / / / / /  
 / / / / / / / / / / / 
/ / / / / / / / / / / /  
 / / / / / / / / / / / 

*/