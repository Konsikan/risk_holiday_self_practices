package question0405;

public class BackShlases {
	// QUESTION 05
	public static void main(String args[]) {
		int num = 10;
		String printingSymbole = "\\";
		for (int t = 1; t <= num; t++) {
			if (t % 2 == 0) {
				for (int i = 1; i <= (num + 1); i++) {
					System.out.print(" " + printingSymbole);
				}
				System.out.println();
			} else {
				for (int i = 1; i <= (num + 2); i++) {
					System.out.print(printingSymbole + " ");
				}
				System.out.println();
			}
		}
	}
}

/*OUTPUT

\ \ \ \ \ \ \ \ \ \ \ \  
 \ \ \ \ \ \ \ \ \ \ \ 
\ \ \ \ \ \ \ \ \ \ \ \  
 \ \ \ \ \ \ \ \ \ \ \ 
\ \ \ \ \ \ \ \ \ \ \ \  
 \ \ \ \ \ \ \ \ \ \ \ 
\ \ \ \ \ \ \ \ \ \ \ \  
 \ \ \ \ \ \ \ \ \ \ \ 
\ \ \ \ \ \ \ \ \ \ \ \  
 \ \ \ \ \ \ \ \ \ \ \  
 
*/