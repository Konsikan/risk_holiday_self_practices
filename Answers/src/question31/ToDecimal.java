package question31;

import java.util.Scanner;

public class ToDecimal {
	// QUESTION 31
	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.print("ENTER THE BINARY NUMBER\t\t: ");
		String binary = scan.nextLine();
		int n=binary.length();
		int sum=0;
		for (int i=n-1; i>=0; i--) {
			if(binary.charAt(i)=='1') {
				sum=sum+(int)Math.pow(2, (n-(i+1)));
			}
		}
		System.out.println("YOUR BINARY NUMBER IN DECIMAL\t: "+sum);
	}
}

/*OUTPUT

ENTER THE BINARY NUMBER		: 1100
YOUR BINARY NUMBER IN DECIMAL	: 12

*/
