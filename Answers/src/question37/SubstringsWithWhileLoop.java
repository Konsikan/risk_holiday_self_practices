package question37;

public class SubstringsWithWhileLoop {
	// QUESTION 37
	public static void main(String[] args) {
		String word = "sebastian-ibis";
		int length = word.length();
		int i = length - 1;
		while (i > 0) {
			System.out.println(word.substring(i));
			i = i - 1;
		}
	}
}

/*OUTPUT

s
is
bis
ibis
-ibis
n-ibis
an-ibis
ian-ibis
tian-ibis
stian-ibis
astian-ibis
bastian-ibis
ebastian-ibis

*/