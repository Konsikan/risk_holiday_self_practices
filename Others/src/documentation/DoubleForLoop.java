package documentation;

/**
 * To Perform Simple Double For Loop Example For practice 
 * --- Output Given Below
 * @author JKonsikan
 * @since 17-05-2020
 * @version 1.0.0v
 */
public class DoubleForLoop {
	/**
	 * variable n, value for looping method
	 */
	public int n = 10;

	public static void main(int n) {
		for (int i = 1; i <= n; i = i + 2) {
			for (int j = 1; j <= n - 1; j++) {
				if (j >= i) {
					System.out.print(j);
				}
			}
			System.out.println();
		}
	}
}
